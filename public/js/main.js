'use strict';

const localVideo = document.getElementById('localVideo');
const remoteVideo = document.getElementById('remoteVideo');

const roomName = document.getElementById('roomName');
const identity = document.getElementById('identity');

const startButton = document.getElementById('startButton');
startButton.addEventListener('click', generateToken, false);

const callButton = document.getElementById('callButton');
callButton.addEventListener('click', startCall, false);
callButton.disabled = true;

const hangupButton = document.getElementById('hangupButton');
hangupButton.addEventListener('click', hangupCall, false);
hangupButton.disabled = true;

let deepConnecting


const videoOnOff = document.getElementById("videoOnOff");



////////////////////////////////////////////////////////////////////////////////
function generateToken() {
  var selectedVideoOnoff;
  for(var i=0; i<videoOnOff.options.length; i++) {
      if(videoOnOff.options[i].selected == true) {
        selectedVideoOnoff = videoOnOff.options[i].value;
        console.log(selectedVideoOnoff)
          break;
      }
  }
  
  // selectedVideoOnoff == (selectedVideoOnoff == 'true')
  // console.log("selected val : ",selectedVideoOnoff)
  // console.log("selected val type : ",typeof(selectedVideoOnoff))
  if(!videoOnOff) return alert('select local video onoff')
  if (!roomName.value) return alert('input room name');
  if (!identity.value) return alert('input identity');




  startButton.disabled = true;

  // Just for demonstration.
  // Save authTokens and get accessToken on on Backend
  const authSid = "gLfvFGpz19aAtUGv05NUXDges7s7OUd1kSElqmhhU";
  const authToken = "LfZ32s1TqJNUJZZh5VKCZMwPcZyumPWpEfdcHD8ngdj8wOfnzJVEBj2hByVPSK"
  axios.post('https://dev.d-connecting.io/api/rooms/tokens', {
    "authSid": authSid,
    "authToken": authToken,
    "identity": identity.value,
    "roomName": roomName.value
  })
    .then(function (response) {
      initDConnecting(authSid, roomName.value, response, selectedVideoOnoff)
    })
    .catch(function (error) {
      startButton.disabled = false
      alert(error);
    });
}
////////////////////////////////////////////////////////////////////////////////

function initDConnecting(authSid, name, response, selectedVideoOnoff) {
  try {
    //When start a new connection, DeepConnecting append video to passed view.
    //We need reset parent view of videos before start connecting.
    const preLocalView = document.getElementById('localVideo')
    while(preLocalView.firstChild){
      preLocalView.removeChild(preLocalView.firstChild)
    }
    const preRemoteView = document.getElementById('remoteVideo')
    while(preRemoteView.firstChild){
      preRemoteView.removeChild(preRemoteView.firstChild)
    }
    const localView = DeepConnecting.initLocal('localVideo')
    const remoteView = DeepConnecting.initLocal('remoteVideo')

    deepConnecting = new DeepConnecting({authSid: authSid, roomName: name, response: response})

    deepConnecting.setReadyListener(readyToCall)
    deepConnecting.connect(function (cb) {
      if (cb) {
        //callee get connection state callback from here
        var stState = new String(cb.state);
        console.log('callback in connect : ',stState)
        if(stState.includes("error")){
          callButton.disabled = false
          alert(stState)
        }
        if (stState == "connected" || stState == "completed"){
          callButton.disabled = true
          hangupButton.disabled = false          
        } 
      } else {
        console.log('connected in mainjs')
        deepConnecting.setSender(localView, selectedVideoOnoff, audioInfo)
        deepConnecting.setReceiver(remoteView)
        roomName.disabled = true
        identity.disabled = true
      }
    })
  }
  catch (e) {
    console.log(e)
    startButton.disabled = false
    roomName.disabled = true
    identity.disabled = true
  }
}


function audioInfo(response){
  
  console.log("audioInfo response :", response)        
}

function readyToCall() {
  console.log('get ready from server')
  callButton.disabled = false
}

function startCall() {
  if (deepConnecting) {
    //Caller get connection callback from startCall function
    deepConnecting.startCall(function (cb) {
      if(cb){
        var stState = new String(cb.state);
        if (stState == "closed" || stState == "failed"){         
                 
        } else if (stState == "disconnected"){
          
        } else if (stState == "connected"){
          
        } else if (stState == "completed"){
         
        }
      }
    })
    callButton.disabled = true
    hangupButton.disabled = false
  }
}

function hangupCall() {
  if (deepConnecting) {
    deepConnecting.hangupCall()
    startButton.disabled = false
    hangupButton.disabled = true
  }
}