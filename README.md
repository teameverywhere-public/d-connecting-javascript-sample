# D-Connecting-Javascript-Sample


Firstly, download our Sample Code.
https://gitlab.com/teameverywhere-public/d-connecting-javascript-sample

And follow to figure out how to use DeepConnecting sdk.
1. Run Sample Code
    : Sample code is built using NodeJS and Express Framework.
    1) Open Code and insert your AuthTokens
    - To use Deep Connecting API, we should get accessToken with authToken and authSid.
    - authToken : Copy AuthToken from Your project.
    - authSid : Copy AuthSid from your project.
    - Don't have a project? -> link
    - Code : 
        const authSid = "your authSid";
        const authToken = "your authToken"

    1) How to Run
        - npm install
        - npm run dev
        - open chrome browser and move to http://localhost:3000

    2) Type roomname and identity
        - room name : Uniqu Room for each Call
                        For example, Alice and Bob is connected on room A.
                        Peter and Steve is connected on room B.
                        In this case, A and B are room names.
        - identiy : your end-user's user ID like email or name.
    3) Click Start button
        - Now you can see your face on monitor
    4) Open Secret Chrome browser and repeat 1) ~ 3)
        - You should insert same roomname and different identity
    5) You just built video chat application.  
       Now let's see detail code.

2. Get DeepConnecting SDK from CDN
    1) Insert scrypto to use DeepConnecting API as Below   
      - <script async src="https://sdk.d-connecting.io/DeepConnecting.js"></script> 

3. Get accessTokn
    1) To init DeepConnecting Object, We need an accessToken expired in 24hours.
    2) insert your authSid and authToken and request Post like below code.    
```javascript
        function generateToken() {      
            axios.post('https://dev.d-connecting.io/api/rooms/tokens', {
                "authSid": authSid,
                "authToken": authToken,
                "identity": identity,
                "roomName": roomName
            })
            .then(function (response) {
                //init DeepConnecting with Response!
                initDConnecting(authSid, roomName, response)
                })
                .catch(function (error) {
                startButton.disabled = false
                alert(error);
            });
        }         
```

4. Init DeepConnecting
    1) Once you got an accessToken, init DeepConnecting object.
    2) localView, remoteView : Custom div tag you generated like in sample Code.
    3) readyListener : If opposit side of connection connect to server, readyListener will be called.
    4) code : 
    ```javascript
        function initDConnecting(authSid, name, response) {
        try {
            const localView = DeepConnecting.initLocal('localVideo')
            const remoteView = DeepConnecting.initLocal('remoteVideo')
            deepConnecting = new DeepConnecting({authSid: authSid, roomName: name, response: response})
            deepConnecting.setReadyListener(readyToCall)
            deepConnecting.connect(function (error) {
                if (error) {
                    callButton.disabled = false
                    alert(error)
                } else {
                    deepConnecting.setSender(localView, audioInfo)
                    deepConnecting.setReceiver(remoteView)                
                }
            })
        }
    ```

5. Start Call
    1) If you can see your face on monitor, You can call to calle.
    ```javascript
    function startCall() {
        if (deepConnecting) {
            //Call startCall function from deepConnecting Object.
            deepConnecting.startCall()
            callButton.disabled = true
            hangupButton.disabled = false
        }   
    }
    ```

6. readyToCall
    1) If you are caller :
        If callee connect same room, call button will be activated.
    2) If you are callee : 
        if you clicked startCall and found your face on monitor, call button will be activated.
    3) Code : 
    ```javascript
        function readyToCall() {
            console.log('get ready from server')
            callButton.disabled = false
        }
    ```

7. Connection callback
    1) If you are caller :
        You get connection callback from below code :
    ```javascript
        function startCall() {
            if (deepConnecting) {
                deepConnecting.startCall(function (cb) {
                if(cb){
                    var stState = new String(cb.state);
                    // console.log("state callback in startCall function : ", stState);
                    if (stState == "closed" || stState == "failed"){         
                            
                    } else if (stState == "disconnected"){
                    
                    } else if (stState == "connected"){
                    
                    } else if (stState == "completed"){
                    
                    }
                }
                })
                callButton.disabled = true
                hangupButton.disabled = false
            }
            }
    ```
    2) If you are callee : 
        You get connection callback from below code :
    ```javascript
            deepConnecting.connect(function (cb) {
            if (cb) {
                var stState = new String(cb.state);
                console.log('callback in connect : ',stState)
                
                if(stState.includes("error")){
                    callButton.disabled = false
                    alert(stState)
                }
                if (stState == "connected" || stState == "completed"){
                    callButton.disabled = true
                    hangupButton.disabled = false          
                } 
                
            }
        ```

7. hangupCall
    : end call

